# Prescriptions API

REST API to insert patient new prescriptions

## Project Architecture
![architecture](images/project_architecture.png "Project Architecture")

- Domain layer define application usecases
- Services layer implements domain usecases
- Infra layer implements services Protocols
- Presentation layer handles requests validation and calls domain usecases
- Main layer make the composition of the other layers

## Prerequisites

For running this project docker and docker-compose need to be installed:

- [Docker](https://docs.docker.com/engine/install/)
- [Docker Compose](https://docs.docker.com/compose/install/)

## Configurations

All project configuration settings are in a **.env** located in project root.

## Deployment

Build and run the project by executing the following command on root directory:
```bash
docker-compose up -d
```
Stop the project by executing the following command on root directory:
```bash
docker-compose down
```


## Usage

With the project running, the following endpoint is available for insert prescriptions:
>http://localhost:5000/prescriptions

For API documentation and Swagger UI use:
>http://localhost:5000/docs

*Request example*
```bash
curl -X POST \
  http://localhost:5000/prescriptions \
  -H 'Content-Type: application/json' \
  -d '{
  "clinic": {
    "id": 1
  },
  "physician": {
    "id": 1
  },
  "patient": {
    "id": 1
  },
  "text": "Dipirona 1x ao dia"
}'
```

*Response.body*
```json
{
  "data": {
    "id": 1,
    "clinic": {
      "id": 1
    },
    "physician": {
      "id": 1
    },
    "patient": {
      "id": 1
    },
    "text": "Dipirona 1x ao dia"
  }
}
```

*Response.body (error)*
```json
{
  "error": {
    "message": "patient not found",
    "code": "03"
  }
}
```
