import random
import pytest
from prescriptions.infra.rest.patients_rest_repository import PatientsRestRepository
from prescriptions.services.protocols.patients_repository import Patient
from prescriptions.config import get_patient_rest_config
from prescriptions.domain.errors import PatientNotFound, PatientsServiceError


def test_return_correct_patient(requests_mock):
    config = get_patient_rest_config()
    patient_id = random.randint(1, 100000000)
    test_patient = {
      'id': str(patient_id),
      'name': "john",
      'email': "john@email.com",
      'phone': "999999999",
    }
    expected = Patient(id=patient_id, name='john', email="john@email.com", phone="999999999")
    patients_rest_repository = PatientsRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{patient_id}", json=test_patient, status_code=200)
    patient = patients_rest_repository.load_by_id(patient_id)
    assert patient == expected


def test_raise_patient_not_found(requests_mock):
    config = get_patient_rest_config()
    patient_id = random.randint(1, 100000000)
    patients_rest_repository = PatientsRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{patient_id}", text='Not Found', status_code=404)
    with pytest.raises(PatientNotFound):
        patients_rest_repository.load_by_id(patient_id)


def test_raise_patient_service_error(requests_mock):
    config = get_patient_rest_config()
    patient_id = random.randint(1, 100000000)
    patients_rest_repository = PatientsRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{patient_id}", text='Not Found', status_code=500)
    with pytest.raises(PatientsServiceError):
        patients_rest_repository.load_by_id(patient_id)


def test_raise_patient_service_error_on_wrong_url():
    patient_id = random.randint(1, 100000000)
    patients_rest_repository = PatientsRestRepository()
    with pytest.raises(PatientsServiceError):
        patients_rest_repository.load_by_id(patient_id)



