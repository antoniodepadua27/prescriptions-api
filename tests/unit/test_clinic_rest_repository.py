import random
from prescriptions.infra.rest.clinics_rest_repository import ClinicsRestRepository
from prescriptions.services.protocols.clinics_repository import Clinic
from prescriptions.config import get_clinic_rest_config


def test_return_correct_clinic(requests_mock):
    config = get_clinic_rest_config()
    clinic_id = random.randint(1, 100000000)
    test_clinic = {
      'id': str(clinic_id),
      'name': 'Test',
    }
    expected = Clinic(id=clinic_id, name='Test')
    clinics_rest_repository = ClinicsRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{clinic_id}", json=test_clinic, status_code=200)
    clinic = clinics_rest_repository.load_by_id(clinic_id)
    assert clinic == expected


def test_return_none_clinic_not_found(requests_mock):
    config = get_clinic_rest_config()
    clinic_id = random.randint(1, 100000000)
    clinics_rest_repository = ClinicsRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{clinic_id}", text='Not Found', status_code=404)
    clinic = clinics_rest_repository.load_by_id(clinic_id)
    assert clinic is None


def test_return_none_on_wrong_url():
    config = get_clinic_rest_config()
    clinic_id = random.randint(1, 100000000)
    clinics_rest_repository = ClinicsRestRepository()
    clinic = clinics_rest_repository.load_by_id(clinic_id)
    assert clinic is None
