import random
import pytest
from prescriptions.infra.rest.physicans_rest_repository import PhysiciansRestRepository
from prescriptions.services.protocols.physicians_repository import Physician
from prescriptions.config import get_physician_rest_config
from prescriptions.domain.errors import PhysiciansNotFound, PhysiciansServiceError


def test_return_correct_physician(requests_mock):
    config = get_physician_rest_config()
    physician_id = random.randint(1, 100000000)
    test_physician = {
      'id': str(physician_id),
      'name': 'Carl',
      'crm': 'e3e23e32e2323e',
    }
    expected = Physician(id=physician_id, name='Carl', crm='e3e23e32e2323e')
    physicians_rest_repository = PhysiciansRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{physician_id}", json=test_physician, status_code=200)
    physician = physicians_rest_repository.load_by_id(physician_id)
    assert physician == expected


def test_raise_physician_not_found(requests_mock):
    config = get_physician_rest_config()
    physician_id = random.randint(1, 100000000)
    physicians_rest_repository = PhysiciansRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{physician_id}", text='Not Found', status_code=404)
    with pytest.raises(PhysiciansNotFound):
        physicians_rest_repository.load_by_id(physician_id)


def test_raise_physician_service_error(requests_mock):
    config = get_physician_rest_config()
    physician_id = random.randint(1, 100000000)
    physicians_rest_repository = PhysiciansRestRepository()
    requests_mock.get(f"{config['host']}/{config['endpoint']}/{physician_id}", text='Not Found', status_code=500)
    with pytest.raises(PhysiciansServiceError):
        physicians_rest_repository.load_by_id(physician_id)


def test_raise_physician_service_error_on_wrong_url():
    physician_id = random.randint(1, 100000000)
    physicians_rest_repository = PhysiciansRestRepository()
    with pytest.raises(PhysiciansServiceError):
        physicians_rest_repository.load_by_id(physician_id)
