from prescriptions.domain.model import AddPrescription, AddPrescriptionParams
from prescriptions.domain.errors import PhysiciansNotFound
from prescriptions.presentation.add_prescriptions_controller import AddPrescriptionsController

DEFAULT_PARAMS = AddPrescriptionParams(text='test', clinic_id=1, patient_id=1, physician_id=1)


class AddPrescriptionFake(AddPrescription):
    def add(self, data: AddPrescriptionParams) -> int:
        if data == DEFAULT_PARAMS:
            return 1
        elif data.physician_id != DEFAULT_PARAMS.physician_id:
            raise PhysiciansNotFound


def test_add_repository_success():
    test_data = {'clinic': {'id': 1}, 'physician': {'id': 1}, 'patient': {'id': 1}, 'text': 'test'}
    expected = {
        'data': {'id': 1, 'clinic': {'id': 1}, 'physician': {'id': 1}, 'patient': {'id': 1}, 'text': 'test'}
    }
    add_prescriptions = AddPrescriptionFake()
    add_prescriptions_controller = AddPrescriptionsController(add_prescriptions)
    result = add_prescriptions_controller.handle(test_data)
    assert (expected, 201) == result


def test_add_repository_raises_not_found():
    test_data = {'clinic': {'id': 1}, 'physician': {'id': 2}, 'patient': {'id': 1}, 'text': 'test'}
    expected = {
                'error': {
                    'message': 'physician not found',
                    'code': '02'
                }
            }
    add_prescriptions = AddPrescriptionFake()
    add_prescriptions_controller = AddPrescriptionsController(add_prescriptions)
    result = add_prescriptions_controller.handle(test_data)
    assert (expected, 400) == result


def test_add_repository_return_malformed_request():
    test_data = {'clinic': {'id': 1}, 'patient': {'id': 1}, 'text': 'test'}
    expected = {
                'error': {
                    'message': 'malformed request',
                    'code': '01'
                }
            }
    add_prescriptions = AddPrescriptionFake()
    add_prescriptions_controller = AddPrescriptionsController(add_prescriptions)
    result = add_prescriptions_controller.handle(test_data)
    assert (expected, 400) == result
