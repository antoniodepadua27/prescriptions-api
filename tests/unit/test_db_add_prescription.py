import pytest
from typing import Optional
from prescriptions.domain.model import AddPrescriptionParams
from prescriptions.domain.errors import PatientNotFound, PhysiciansNotFound
from prescriptions.services.usecases.db_add_prescription import DbAddPrescription
from prescriptions.services.protocols.prescriptions_repository import PrescriptionsRepository
from prescriptions.services.protocols.prescriptions_session import PrescriptionsSession
from prescriptions.services.protocols.patients_repository import PatientsRepository, Patient
from prescriptions.services.protocols.physicians_repository import PhysiciansRepository, Physician
from prescriptions.services.protocols.clinics_repository import ClinicsRepository, Clinic
from prescriptions.services.protocols.metrics_repository import MetricsRepository, MetricParams


class PrescriptionsRepositoryFake(PrescriptionsRepository):

    def add(self, data: AddPrescriptionParams) -> int:
        return 1


class ClinicsRepositoryFake(ClinicsRepository):

    def load_by_id(self, clinic_id: int) -> Optional[Clinic]:
        clinic = Clinic(id=1, name='Test')
        if clinic.id == clinic_id:
            return clinic
        else:
            return None


class PatientsRepositoryFake(PatientsRepository):

    def load_by_id(self, patient_id: int) -> Patient:
        patient = Patient(id=1, name='john', email='john@email.com', phone='99999999')
        if patient.id == patient_id:
            return patient
        else:
            raise PatientNotFound


class PhysiciansRepositoryFake(PhysiciansRepository):

    def load_by_id(self, physician_id: int) -> Physician:
        physician = Physician(id=1, name='Carl', crm='5f5f2121')
        if physician.id == physician_id:
            return physician
        else:
            raise PhysiciansNotFound


DEFAULT_METRICS = MetricParams(
    clinic_id=1,
    clinic_name='Test',
    patient_id=1,
    patient_name='john',
    patient_email='john@email.com',
    patient_phone='99999999',
    physician_id=1,
    physician_name='Carl',
    physician_crm='5f5f2121'
)


class MetricsRepositoryFake(MetricsRepository):

    def add(self, data: MetricParams):
        return True


class PrescriptionsSessionFake(PrescriptionsSession):
    def __init__(self):
        self.prescriptions_repository = PrescriptionsRepositoryFake()
        self.committed = False

    def commit(self):
        self.committed = True

    def rollback(self):
        pass


def fake_add_prescriptions_factory():
    clinics_repository = ClinicsRepositoryFake()
    prescriptions_session = PrescriptionsSessionFake()
    patients_repository = PatientsRepositoryFake()
    physicians_repository = PhysiciansRepositoryFake()
    metrics_repository = MetricsRepositoryFake()
    db_add_prescription = DbAddPrescription(
        prescriptions_session,
        clinics_repository,
        patients_repository,
        physicians_repository,
        metrics_repository,
    )
    return db_add_prescription, prescriptions_session


def test_add_prescription():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=1, physician_id=1)
    result = db_add_prescription.add(add_prescription_params)
    assert result == 1
    assert prescriptions_session.committed


def test_patient_repository_return_correct_patient():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=1, physician_id=1)
    db_add_prescription.add(add_prescription_params)
    assert prescriptions_session.committed


def test_patient_repository_raises_patient_not_found():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=2, physician_id=1)
    with pytest.raises(PatientNotFound):
        db_add_prescription.add(add_prescription_params)
    assert not prescriptions_session.committed


def test_physician_repository_return_correct_physician():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=1, physician_id=1)
    db_add_prescription.add(add_prescription_params)
    assert prescriptions_session.committed


def test_physician_repository_raises_physician_not_found():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=1, physician_id=2)
    with pytest.raises(PhysiciansNotFound):
        db_add_prescription.add(add_prescription_params)
    assert not prescriptions_session.committed


def test_clinic_repository_return_correct_clinic():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=1, physician_id=1)
    db_add_prescription.add(add_prescription_params)
    assert prescriptions_session.committed


def test_clinic_repository_do_nothing_clinic_not_found():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=2, patient_id=1, physician_id=1)
    db_add_prescription.add(add_prescription_params)
    assert prescriptions_session.committed


def test_metrics_repository_add_success():
    db_add_prescription, prescriptions_session = fake_add_prescriptions_factory()
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=1, physician_id=1)
    db_add_prescription.add(add_prescription_params)
    assert prescriptions_session.committed
