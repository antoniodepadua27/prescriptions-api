import pytest
from prescriptions.domain.errors import MetricsServiceError
from prescriptions.infra.rest.metrics_rest_repository import MetricsRestRepository
from prescriptions.services.protocols.metrics_repository import MetricParams
from prescriptions.config import get_metric_rest_config

DEFAULT_METRICS = MetricParams(
    clinic_id=1,
    clinic_name='Test',
    patient_id=1,
    patient_name='john',
    patient_email='john@email.com',
    patient_phone='99999999',
    physician_id=1,
    physician_name='Carl',
    physician_crm='5f5f2121'
)


def test_raise_metrics_service_error_on_wrong_url():
    metrics_rest_repository = MetricsRestRepository()
    with pytest.raises(MetricsServiceError):
        metrics_rest_repository.add(DEFAULT_METRICS)


def test_raise_metrics_service_error(requests_mock):
    config = get_metric_rest_config()
    metrics_rest_repository = MetricsRestRepository()
    requests_mock.post(f"{config['host']}/{config['endpoint']}", status_code=500)
    with pytest.raises(MetricsServiceError):
        metrics_rest_repository.add(DEFAULT_METRICS)
