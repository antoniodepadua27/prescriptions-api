import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from prescriptions.infra.db.orm import Base


@pytest.fixture
def in_memory_db():
    engine = create_engine("sqlite:///:memory:")
    Base.metadata.create_all(engine)
    return engine


@pytest.fixture
def session_factory(in_memory_db):
    yield sessionmaker(bind=in_memory_db)


@pytest.fixture
def session(session_factory):
    return session_factory()
