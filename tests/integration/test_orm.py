from prescriptions.infra.db.orm import Prescriptions


def test_prescription_is_correct_save(session):
    prescription = Prescriptions(
        clinic_id=1,
        patient_id=2,
        physician_id=3,
        text='test'
    )
    session.add(prescription)
    session.commit()
    rows = list(session.execute('SELECT clinic_id, patient_id, physician_id FROM prescriptions'))
    assert rows == [(1, 2, 3)]
