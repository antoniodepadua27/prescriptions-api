from prescriptions.infra.db.prescriptions_sqlalchemy_repository import PrescriptionsSqlAlchemyRepository
from prescriptions.domain.model import AddPrescriptionParams


def test_prescription_repository_can_save(session):
    repository = PrescriptionsSqlAlchemyRepository(session)
    add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=2, physician_id=3)
    prescription_id = repository.add(add_prescription_params)
    session.commit()
    rows = list(session.execute(
        f'SELECT clinic_id, patient_id, physician_id FROM prescriptions WHERE id={prescription_id}'
    ))
    assert rows == [(1, 2, 3)]
