import pytest
from prescriptions.infra.db.prescriptions_sqlachemy_session import PrescriptionsSqlAlchemySession
from prescriptions.domain.model import AddPrescriptionParams


def test_prescription_session_can_save(session_factory):
    prescriptions_session = PrescriptionsSqlAlchemySession(session_factory)
    with prescriptions_session:
        add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=2, physician_id=3)
        prescriptions_session.prescriptions_repository.add(add_prescription_params)
        prescriptions_session.commit()
    new_session = session_factory()
    rows = list(new_session.execute('SELECT clinic_id, patient_id, physician_id FROM prescriptions'))
    assert rows == [(1, 2, 3)]


def test_prescription_session_not_commit_by_default(session_factory):
    prescriptions_session = PrescriptionsSqlAlchemySession(session_factory)
    with prescriptions_session:
        add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=2, physician_id=3)
        prescriptions_session.prescriptions_repository.add(add_prescription_params)
    new_session = session_factory()
    rows = list(new_session.execute('SELECT clinic_id, patient_id, physician_id FROM prescriptions'))
    assert rows == []


def test_prescription_session_roll_back(session_factory):
    prescriptions_session = PrescriptionsSqlAlchemySession(session_factory)
    with pytest.raises(Exception):
        with prescriptions_session:
            add_prescription_params = AddPrescriptionParams(text='test', clinic_id=1, patient_id=2, physician_id=3)
            prescriptions_session.prescriptions_repository.add(add_prescription_params)
            raise Exception
    new_session = session_factory()
    rows = list(new_session.execute('SELECT clinic_id, patient_id, physician_id FROM prescriptions'))
    assert rows == []
