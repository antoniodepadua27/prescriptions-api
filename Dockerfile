FROM python:3.8-buster

COPY requirements.txt /tmp/
RUN pip install -r /tmp/requirements.txt

WORKDIR /src

COPY src/ /src/
RUN pip install -e /src

ENV FLASK_APP=prescriptions.main.flask_app.py FLASK_DEBUG=0 PYTHONUNBUFFERED=1
CMD flask run --host=0.0.0.0 --port=80