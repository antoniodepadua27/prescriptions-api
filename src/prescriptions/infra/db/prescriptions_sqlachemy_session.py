from sqlalchemy.orm import sessionmaker
from prescriptions.services.protocols.prescriptions_session import PrescriptionsSession
from .prescriptions_sqlalchemy_repository import PrescriptionsSqlAlchemyRepository
from .orm import ENGINE

DEFAULT_SESSION_FACTORY = sessionmaker(bind=ENGINE)


class PrescriptionsSqlAlchemySession(PrescriptionsSession):
    """PrescriptionsSession implementation using Sqlalchemy"""
    def __init__(self, session_factory=DEFAULT_SESSION_FACTORY):
        self.session_factory = session_factory

    def __enter__(self):
        self.session = self.session_factory()
        self.prescriptions_repository = PrescriptionsSqlAlchemyRepository(self.session)
        return super().__enter__()

    def __exit__(self, *args):
        super().__exit__(*args)
        self.session.close()

    def commit(self):
        self.session.commit()

    def rollback(self):
        self.session.rollback()
