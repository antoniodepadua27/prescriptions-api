from prescriptions.services.protocols.prescriptions_repository import PrescriptionsRepository
from prescriptions.domain.model import AddPrescriptionParams
from .orm import Prescriptions
from sqlalchemy.orm import Session


class PrescriptionsSqlAlchemyRepository(PrescriptionsRepository):
    """PrescriptionsRepository implementation using Sqlalchemy"""

    def __init__(self, session: Session):
        self.session = session

    def add(self, data: AddPrescriptionParams) -> int:
        prescription = Prescriptions(
            text=data.text,
            clinic_id=data.clinic_id,
            patient_id=data.patient_id,
            physician_id=data.physician_id
        )
        self.session.add(prescription)
        self.session.flush()
        return prescription.id
