from datetime import datetime
from sqlalchemy import create_engine
from sqlalchemy import Column, Integer, String, DateTime
from sqlalchemy.ext.declarative import declarative_base
from prescriptions.config import get_postgres_uri

Base = declarative_base()

ENGINE = create_engine(
    get_postgres_uri()
)


class Prescriptions(Base):
    __tablename__ = 'prescriptions'
    id: int = Column(Integer, primary_key=True)
    text: str = Column(String, nullable=False)
    clinic_id: int = Column(Integer, nullable=False)
    patient_id: int = Column(Integer, nullable=False)
    physician_id: int = Column(Integer, nullable=False)
    created_at: datetime = Column(DateTime, default=datetime.utcnow())
