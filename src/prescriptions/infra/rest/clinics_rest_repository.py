import logging
from typing import Optional
from requests_cache import CachedSession
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry, MaxRetryError
from requests.exceptions import RequestException
from prescriptions.config import get_clinic_rest_config
from prescriptions.services.protocols.clinics_repository import ClinicsRepository, Clinic


class ClinicsRestRepository(ClinicsRepository):
    def load_by_id(self, clinic_id: int) -> Optional[Clinic]:
        configs = get_clinic_rest_config()
        url = f"{configs['host']}/{configs['endpoint']}/{clinic_id}"
        authorization = {'Authorization': configs['auth']}
        retry_strategy = Retry(
            total=configs['retry'],
            status_forcelist=[429, 500, 502, 503, 504],
            method_whitelist=["GET"]
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)
        clinic = None
        try:
            with CachedSession(backend='sqlite', expire_after=configs['cache_ttl']) as session:
                session.mount('https://', adapter)
                response = session.get(url, headers=authorization, timeout=configs['timeout'])
                response.raise_for_status()
                data = response.json()
                clinic = Clinic(
                    id=int(data['id']),
                    name=data['name'],
                )
                return clinic
        except (RequestException, MaxRetryError) as e:
            logging.error(e, exc_info=True)
        return clinic
