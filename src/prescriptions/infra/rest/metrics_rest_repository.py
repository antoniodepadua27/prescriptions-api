import requests
import logging
from dataclasses import asdict
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry, MaxRetryError
from requests.exceptions import RequestException
from prescriptions.services.protocols.metrics_repository import MetricsRepository, MetricParams
from prescriptions.config import get_metric_rest_config
from prescriptions.domain.errors import MetricsServiceError


class MetricsRestRepository(MetricsRepository):
    def add(self, data: MetricParams):
        configs = get_metric_rest_config()
        url = f"{configs['host']}/{configs['endpoint']}"
        authorization = {'Authorization': configs['auth']}
        retry_strategy = Retry(
            total=configs['retry'],
            status_forcelist=[429, 500, 502, 503, 504],
            method_whitelist=["POST"]
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)
        try:
            with requests.Session() as session:
                session.mount('https://', adapter)
                response = session.post(url, data=asdict(data), headers=authorization, timeout=configs['timeout'])
                response.raise_for_status()

        except (RequestException, MaxRetryError) as e:
            logging.error(e, exc_info=True)
            raise MetricsServiceError
