import logging
from requests_cache import CachedSession
from requests.adapters import HTTPAdapter
from requests.packages.urllib3.util.retry import Retry, MaxRetryError
from requests.exceptions import HTTPError, ConnectionError, Timeout, RetryError
from prescriptions.config import get_patient_rest_config
from prescriptions.domain.errors import PatientNotFound, PatientsServiceError
from prescriptions.services.protocols.patients_repository import PatientsRepository, Patient


class PatientsRestRepository(PatientsRepository):
    def load_by_id(self, patient_id: int) -> Patient:
        configs = get_patient_rest_config()
        url = f"{configs['host']}/{configs['endpoint']}/{patient_id}"
        authorization = {'Authorization': configs['auth']}
        retry_strategy = Retry(
            total=configs['retry'],
            status_forcelist=[429, 500, 502, 503, 504],
            method_whitelist=["GET"]
        )
        adapter = HTTPAdapter(max_retries=retry_strategy)
        try:
            with CachedSession(backend='sqlite', expire_after=configs['cache_ttl']) as session:
                session.mount('https://', adapter)
                response = session.get(url, headers=authorization, timeout=configs['timeout'])
                response.raise_for_status()
                data = response.json()
                patient = Patient(
                    id=int(data['id']),
                    name=data['name'],
                    email=data['email'],
                    phone=data['phone'],
                )
                return patient
        except HTTPError as e:
            logging.error(e, exc_info=True)
            if e.response.status_code == 404:
                raise PatientNotFound
            else:
                raise PatientsServiceError
        except (Timeout, RetryError, ConnectionError, MaxRetryError) as e:
            logging.error(e, exc_info=True)
            raise PatientsServiceError
