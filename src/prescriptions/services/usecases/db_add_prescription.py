from prescriptions.domain.model import AddPrescription, AddPrescriptionParams
from prescriptions.services.protocols.prescriptions_session import PrescriptionsSession
from prescriptions.services.protocols.patients_repository import PatientsRepository
from prescriptions.services.protocols.physicians_repository import PhysiciansRepository
from prescriptions.services.protocols.clinics_repository import ClinicsRepository
from prescriptions.services.protocols.metrics_repository import MetricsRepository, MetricParams


class DbAddPrescription(AddPrescription):
    """Implementation of AddPrescription domain usecase"""
    def __init__(self,
                 prescriptions_session: PrescriptionsSession,
                 clinics_repository: ClinicsRepository,
                 patients_repository: PatientsRepository,
                 physicians_repository: PhysiciansRepository,
                 metrics_repository: MetricsRepository):
        self.prescriptions_session = prescriptions_session
        self.clinics_repository = clinics_repository
        self.patients_repository = patients_repository
        self.physicians_repository = physicians_repository
        self.metrics_repository = metrics_repository

    def add(self, data: AddPrescriptionParams) -> int:
        with self.prescriptions_session:
            clinic = self.clinics_repository.load_by_id(data.clinic_id)
            patient = self.patients_repository.load_by_id(data.patient_id)
            physician = self.physicians_repository.load_by_id(data.physician_id)
            prescription_id = self.prescriptions_session.prescriptions_repository.add(data)
            self.metrics_repository.add(
                MetricParams(
                    clinic_id=data.clinic_id,
                    clinic_name=clinic.name if clinic else '',
                    physician_id=physician.id,
                    physician_name=physician.name,
                    physician_crm=physician.crm,
                    patient_id=patient.id,
                    patient_name=patient.name,
                    patient_email=patient.email,
                    patient_phone=patient.phone
                )
            )
            self.prescriptions_session.commit()
            return prescription_id
