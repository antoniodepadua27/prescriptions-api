import abc
from dataclasses import dataclass
from typing import Optional


@dataclass
class Clinic:
    id: int
    name: str


class ClinicsRepository(abc.ABC):
    """Abstract clinics repository to be implemented in infra layer """
    @abc.abstractmethod
    def load_by_id(self, clinic_id: int) -> Optional[Clinic]:
        raise NotImplementedError
