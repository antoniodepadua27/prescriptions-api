import abc
from dataclasses import dataclass


@dataclass
class MetricParams:
    clinic_id: int
    clinic_name: str
    physician_id: int
    physician_name: str
    physician_crm: str
    patient_id: int
    patient_name: str
    patient_email: str
    patient_phone: str


class MetricsRepository(abc.ABC):
    """Abstract metrics repository to be implemented in infra layer """
    @abc.abstractmethod
    def add(self, data: MetricParams):
        raise NotImplementedError
