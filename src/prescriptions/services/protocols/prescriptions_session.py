import abc
from .prescriptions_repository import PrescriptionsRepository


class PrescriptionsSession(abc.ABC):
    """Interface for transaction control abstraction
    Attributes:
        prescriptions_repository: The repository controlled by the transaction.
    """
    prescriptions_repository: PrescriptionsRepository

    def __enter__(self):
        return self

    def __exit__(self, *args):
        self.rollback()

    @abc.abstractmethod
    def commit(self):
        raise NotImplementedError

    @abc.abstractmethod
    def rollback(self):
        raise NotImplementedError
