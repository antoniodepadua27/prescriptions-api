import abc
from prescriptions.domain.model import AddPrescriptionParams


class PrescriptionsRepository(abc.ABC):
    """Abstract prescriptions repository to be implemented in infra layer """
    @abc.abstractmethod
    def add(self, data: AddPrescriptionParams) -> int:
        raise NotImplementedError
