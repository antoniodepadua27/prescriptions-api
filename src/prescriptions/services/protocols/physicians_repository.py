import abc
from dataclasses import dataclass


@dataclass
class Physician:
    id: int
    name: str
    crm: str


class PhysiciansRepository(abc.ABC):
    """Abstract physicians repository to be implemented in infra layer """
    @abc.abstractmethod
    def load_by_id(self, physician_id: int) -> Physician:
        raise NotImplementedError
