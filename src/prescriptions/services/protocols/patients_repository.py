import abc
from dataclasses import dataclass


@dataclass
class Patient:
    id: int
    name: str
    email: str
    phone: str


class PatientsRepository(abc.ABC):
    """Abstract patients repository to be implemented in infra layer """
    @abc.abstractmethod
    def load_by_id(self, patient_id: int) -> Patient:
        raise NotImplementedError
