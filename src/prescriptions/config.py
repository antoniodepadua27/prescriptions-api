import os


def get_patient_rest_config() -> dict:
    return {
        'host': os.environ.get('PATIENT_HOST', 'https://localhost'),
        'endpoint': os.environ.get('PATIENT_ENDPOINT', 'patients'),
        'timeout': int(os.environ.get('PATIENT_TIMEOUT', 3)),
        'cache_ttl': int(os.environ.get('PATIENT_CACHE_TTL', 43200)),
        'retry': int(os.environ.get('PATIENT_RETRY', 2)),
        'auth': os.environ.get('PATIENT_AUTH', ''),
    }


def get_physician_rest_config() -> dict:
    return {
        'host': os.environ.get('PHYSICIAN_HOST', 'https://localhost'),
        'endpoint': os.environ.get('PHYSICIAN_ENDPOINT', 'physicians'),
        'timeout': int(os.environ.get('PHYSICIAN_TIMEOUT', 4)),
        'cache_ttl': int(os.environ.get('PHYSICIAN_CACHE_TTL', 172800)),
        'retry': int(os.environ.get('PHYSICIAN_RETRY', 2)),
        'auth': os.environ.get('PHYSICIAN_AUTH', ''),
    }


def get_clinic_rest_config() -> dict:
    return {
        'host': os.environ.get('CLINIC_HOST', 'https://localhost'),
        'endpoint': os.environ.get('CLINIC_ENDPOINT', 'clinics'),
        'timeout': int(os.environ.get('CLINIC_TIMEOUT', 5)),
        'cache_ttl': int(os.environ.get('CLINIC_CACHE_TTL', 259200)),
        'retry': int(os.environ.get('CLINIC_RETRY', 3)),
        'auth': os.environ.get('CLINIC_AUTH', ''),
    }


def get_metric_rest_config() -> dict:
    return {
        'host': os.environ.get('METRIC_HOST', 'https://localhost'),
        'endpoint': os.environ.get('METRIC_ENDPOINT', 'metrics'),
        'timeout': int(os.environ.get('METRIC_TIMEOUT', 6)),
        'retry': int(os.environ.get('METRIC_RETRY', 5)),
        'auth': os.environ.get('METRIC_AUTH', ''),
    }


def get_postgres_uri():
    host = os.environ.get("DB_HOST", "postgres")
    port = os.environ.get("DB_PORT", 5432)
    password = os.environ.get("DB_PASSWORD", "123456")
    user = os.environ.get("DB_USER", "prescriptions")
    db_name = os.environ.get("DB_NAME", "prescriptions")
    return f"postgresql://{user}:{password}@{host}:{port}/{db_name}"
