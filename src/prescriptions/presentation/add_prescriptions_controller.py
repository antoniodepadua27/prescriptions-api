from typing import Tuple
from cerberus import Validator
from prescriptions.domain.errors import PrescriptionsError, MalformedRequest
from prescriptions.domain.model import AddPrescription, AddPrescriptionParams

add_prescriptions_schema = {
        'text': {'type': 'string', 'required': True},
        'clinic': {
            'type': 'dict',
            'required': True,
            'schema': {
                'id': {'type': 'number', 'required': True}
            }
        },
        'patient': {
            'type': 'dict',
            'required': True,
            'schema': {
                'id': {'type': 'number', 'required': True}
            }
        },
        'physician': {
            'type': 'dict',
            'required': True,
            'schema': {
                'id': {'type': 'number', 'required': True}
            }
        },
}


class AddPrescriptionsController:
    def __init__(self, add_prescription: AddPrescription):
        self.add_prescription = add_prescription

    def handle(self, data: dict) -> Tuple[dict, int]:
        try:
            validator = Validator(add_prescriptions_schema)
            if not validator.validate(data):
                raise MalformedRequest
            prescription_id = self.add_prescription.add(
                AddPrescriptionParams(
                    patient_id=data['patient']['id'],
                    physician_id=data['physician']['id'],
                    clinic_id=data['clinic']['id'],
                    text=data['text'])
            )
            print(prescription_id)
            data['id'] = prescription_id
            return {'data': data}, 201
        except PrescriptionsError as e:
            error = {
                'error': {
                    'message': e.message,
                    'code': e.code
                }
            }
            return error, 400
