import abc
from dataclasses import dataclass


@dataclass
class AddPrescriptionParams:
    text: str
    clinic_id: int
    physician_id: int
    patient_id: int


class AddPrescription(abc.ABC):

    @abc.abstractmethod
    def add(self, data: AddPrescriptionParams) -> int:
        raise NotImplementedError
