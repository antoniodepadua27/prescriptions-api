class PrescriptionsError(Exception):
    code = ""
    message = ""


class MalformedRequest(PrescriptionsError):
    code = '01'
    message = 'malformed request'


class PhysiciansNotFound(PrescriptionsError):
    code = '02'
    message = 'physician not found'


class PatientNotFound(PrescriptionsError):
    code = '03'
    message = 'patient not found'


class MetricsServiceError(PrescriptionsError):
    code = '04'
    message = 'metrics service not available'


class PhysiciansServiceError(PrescriptionsError):
    code = '05'
    message = 'physicians service not available'


class PatientsServiceError(PrescriptionsError):
    code = '06'
    message = 'patients service not available'




