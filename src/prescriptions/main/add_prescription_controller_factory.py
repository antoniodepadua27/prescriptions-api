from prescriptions.presentation.add_prescriptions_controller import AddPrescriptionsController
from prescriptions.services.usecases.db_add_prescription import DbAddPrescription
from prescriptions.infra.db.prescriptions_sqlachemy_session import PrescriptionsSqlAlchemySession
from prescriptions.infra.rest.clinics_rest_repository import ClinicsRestRepository
from prescriptions.infra.rest.patients_rest_repository import PatientsRestRepository
from prescriptions.infra.rest.physicans_rest_repository import PhysiciansRestRepository
from prescriptions.infra.rest.metrics_rest_repository import MetricsRestRepository


def add_prescription_controller_factory() -> AddPrescriptionsController:
    prescriptions_sqlalchemy_session = PrescriptionsSqlAlchemySession()
    clinics_rest_repository = ClinicsRestRepository()
    patients_rest_repository = PatientsRestRepository()
    physicians_rest_repository = PhysiciansRestRepository()
    metrics_rest_repository = MetricsRestRepository()
    db_add_prescription = DbAddPrescription(
        prescriptions_sqlalchemy_session,
        clinics_rest_repository,
        patients_rest_repository,
        physicians_rest_repository,
        metrics_rest_repository
    )
    return AddPrescriptionsController(db_add_prescription)
