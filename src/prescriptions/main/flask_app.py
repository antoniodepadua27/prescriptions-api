from flask import Flask, request
from flask_swagger_ui import get_swaggerui_blueprint
from .add_prescription_controller_factory import add_prescription_controller_factory
from prescriptions.infra.db.orm import Base, ENGINE

app = Flask(__name__)

Base.metadata.create_all(ENGINE)

SWAGGER_URL = '/docs'
API_URL = '/static/swagger.json'
SWAGGER_UI_BLUEPRINT = get_swaggerui_blueprint(
    SWAGGER_URL,
    API_URL,
    config={
        'app_name': "Prescriptions-API"
    }
)
app.register_blueprint(SWAGGER_UI_BLUEPRINT, url_prefix=SWAGGER_URL)


@app.route("/prescriptions", methods=['POST'])
def add_prescription():
    data = request.json
    add_prescription_controller = add_prescription_controller_factory()
    response = add_prescription_controller.handle(data)
    return response
